## Sensors and Actuators
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/1.png)  
### Sensors  
- A transducer is any physical device that converts one form of energy into another.
- Sensor is, the transducer converts some physical energy into an electrical
signal that can then be used to take a reading.
- Example: A microphone is a sensor that takes vibrational energy (sound waves),
and converts it to electrical signal for the system to relate it back to the original sound.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/1.11.jpg)  
- Examples : ![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/top_sensor_types_used_in_iot-04.png)
### Actuators
- Actuator is another type of transducer which operates in reverse direction of a sensor. Actuator takes an electrical input and turns it into physical action.
- Examples:
![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/top_sensor_types_used_in_iot-05.png)
  ![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/top_sensor_types_used_in_iot-02.png)
## Analog and Digital 
- Usually Analog(continuos values over time) and Digital signals(For every time instant only 2 values.0/1) are used to transmit data through electric signals.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/2.jpg)
![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/analogvsdigital.jpg)
## Micro controllers Vs Micro processors
- The most popular control boards for the project are Arduino and Raspberry Pi.
- Let's consider **Arduino Uno** it has RAM:2K,Flash:32K,Timers,Serial(UART),I2C,SPI.
- The code that is written in IDE will be the only one that work on the chip. The C code is compiled into machine language and runs ont the arduino itself.  
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/3.1.jpg) ![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/3.2.jpg)
- Let's consider **Raspberry Pi** 
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/3.3.jpg)   
- It's more common with our computer.Unlike Arduino,In Raspberry Pi we actually write programs that run with in an operating system.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/3.jpg)  
- Arduino is a microcontroller and Raspberry Pi is microprocessor. Both are very useful but which one do we want to use depends on our project.
- For example Arduino can control easily sensors,character LCDs,motor etc.Where as for projects involving camera,video,complex math,graphic interfaces RaspberryPi is best suited. 
- Arduino is widely used for controlling things and Raspberry Pi for processing lots of data.
## Raspberry Pi
- As we can see the figure of Raspberry Pi above, It has GPIO,UART,SPI,I2C,PWM.These are interfaces. Interfaces are ways to connect a sensor to a microprocessor.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/4.jpg)
- Raspberry Pi is a   
  - Mini Computer
  - Limited but large power for its size
  - No storage
  - It is a SOC (System On Chip)
  - We can connect shields (Shields - addon functionalities)
  - Can connect multiple Pi’s together
  - Microprocessor
  - Can load a linux OS on it
  - Pi uses ARM
  - Connect to sensors or actuators
## Series and Parallel
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/4.2.jpg)
### Parallel Interfaces:
- GPIO (GPIO stands for General Purpose Input/Output. It's a standard interface used to connect microcontrollers to other electronic devices.)
### Serial Interfaces:
- UART(Universal Asynchronous Receiver/Transmitter)
- SPI (The Serial Peripheral Interface)
- I2C