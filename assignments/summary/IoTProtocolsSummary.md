## IOT Protocols

### 4-20mA
- Of all possible analog signals that can be used to transmit process information, the 4-20 mA loop is, by far, the dominant standard in the industry.
- The main law we use here is Ohm's law and current remains constant in the given circuit.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/6.1.jpg)  
- In electrical signalling an analog current loop is used where a device must be monitored or controlled remotely over a pair of conductors.A major application of current loops is the industry de facto standard 4–20 mA current loop for process control applications, where they are extensively used to carry signals from process instrumentation to PID controllers, SCADA systems, and programmable logic controllers (PLCs).  
**Components:**
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/6.jpg)  
1. Sensor:
    - A sensor typically measures temperature, humidity, flow, level or pressure. 
2. Transmitter
    - Transmitter converts physical quantity measured by sensor into current signal between 4-20 mA.
3. Power Source
    - In order for a signal to be produced, there needs to be a source of power.
    - There are many common voltages that are used with 4-20 mA current loops (9, 12, 24, etc.) depending on the particular setup. When deciding on what voltage of power supply to use for your particular setup, be sure to consider that the power supply voltage must be at least 10% greater than the total voltage drop of the attached components (the transmitter, receiver and even wire).
    - The power supply must output a DC current (meaning that the current is only flowing in one direction).
4. Loop
    - It is the actual wire connecting the sensor to the device receiving the 4-20 mA signal and then back to the transmitter.
    - The current signal on the loop is regulated by the transmitter according to the sensor's measurement.
5. Receiver
    - A device which can receive and interpret the current signal.
- Every process has Pros and Cons.Let's see some of them  
* **Pros**
    - It is the simplest option to connect and configure. Also low initial setup cost.It is less sensitive to background electrical noise.
    - Better for traveling long distances, as current does not degrade over long connections like voltage.
* **Cons**
    - Current loops can only transmit one particular process signal.
    - Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to problems with ground loops if independent loops are not properly isolated.These isolation requirements become exponentially more complicated as the number of loops increases.
## Modbus Communication Protocol
- Modbus is a data communications protocol originally published by Modicon (now Schneider Electric) in 1979 for use with its programmable logic controllers (PLCs).
- It is used to transmit signals from instrumentation and control devices back to main controller,or data gathering system.This method is used for transmitting information between electronic devices.  
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/7.jpg)   
**Master:** The device requesting information; 
**Slaves:** the devices supplying information.  
- In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.Communication between a master and a slave occurs in a frame that indicates a function code.
- The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
The slave then responds, based on the function code received.
- Modbus is used as local interface to manage devices.It can be used over two interfaces.
    - RS485 - called as Modbus RTU
    - Ethernet - called as Modbus TCP/IP
### RS485
- It is a serial transmission standard. i.e., we can put several RS485 devices on the same bus.(RS485 is not directly compatible.)
- We have to use the correct type of interface.It is done through RS485 to USB.
## OPC UA 
- OPC UA (short for Open Platform Communications United Architecture) is a data exchange standard for industrial communication (machine-to-machine or PC-to-machine communication).
- Security – OPC UA implements a sophisticated Security Model that ensures the authentication of Client and Servers, the authentication of users and the integrity of their communication. ... OPC UA is designed to connect Objects in such a way that true Information can be shared between Clients and Servers.
![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/5919e2325a45423a158c276511dde466/opcua1.png)
### OPC UA Client - Server  :
![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/d24af13157b45bd8102a6d4e278a007f/opcua2.png)
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/8.jpg)
## Cloud Protocols

### MQTT (Message Queuing Telemetry Transport) :
- A simple messaging protocol designed for constrained devices with low bandwidth.
- MQTT (MQ Telemetry Transport or Message Queuing Telemetry Transport) is an open OASIS and ISO standard (ISO/IEC 20922) lightweight, publish-subscribe network protocol that transports messages between devices.
- It is a perfect solution for IOT applications.It also allows to send commands to control outputs,read and publish data from sensor nodes.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/9.jpg)
- MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
- Topics are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.
![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/358848b4b9222917e31db143ff0a6383/Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol.png)
## HTTP (Hyper Text Transport Protocol)
- The Hypertext Transfer Protocol (HTTP) is an application layer protocol for distributed, collaborative, hypermedia information systems.HTTP is the foundation of data communication for the World Wide Web, where hypertext documents include hyperlinks to other resources that the user can easily access, for example by a mouse click or by tapping the screen in a web browser.
![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/85f443d40e36e1f5bcb4e71ac14205b0/http-req-res.png)  
- It is a request response protocol.The client sends an HTTP request.The server sends back a HTTP response.
1. Request 
    - Request line
    - HTTP headers
    - message body  
    **GET request**
it is a type of HTTP request using the GET method
- GET
     Retrieve the resource from the server (e.g. when visiting a page);
- POST
     Create a resource on the server (e.g. when submitting a form);
- PUT/PATCH
     Update the resource on the server (used by APIs);
- DELETE
     Delete the resource from the server (used by APIs).

2. The response is made of three lines.
    - Status line
    - HTTP header
    - Message body
![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/1dcbcc2c7e58e59a042e55b2b2080405/restful-web-services-with-spring-mvc-28-638.jpg)








