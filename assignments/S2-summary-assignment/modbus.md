## Modbus
- Modbus is a communication protocol developed by Modicon systems. In simple terms, it is a method used for transmitting information over serial lines between electronic devices. The device requesting the information is called the Modbus Master and the devices supplying information are Modbus Slaves.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/7.jpg)
### Working
- The Modbus RTU protocol uses a Master/Slave technique to communicate between devices. Meaning, any application that utilizes the Modbus RTU protocol will have a Modbus Master and at least one Modbus Slave
  
Also written in IoTProtocols summary