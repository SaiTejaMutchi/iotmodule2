## Twilio
- Twilio (/ˈtwɪlioʊ/) is an American cloud communications platform as a service (CPaaS) company based in San Francisco, California.
- Twilio allows software developers to programmatically make and receive phone calls, send and receive text messages, and perform other communication functions using its web service APIs.
- Twilio IoT provides global cellular connectivity for your Internet of Things devices.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/133.jpg)
### Working
- Twilio IoT connectivity is delivered through Twilio SIM cards and modules that are fully manageable through our software APIs. With Twilio SIMs, you can go from prototype to production with CAT-M1, LTE, Narrowband, and 2G/3G.  
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/134.jpg)
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/Why_Mobile_Messaging_Works_Infographic.png)