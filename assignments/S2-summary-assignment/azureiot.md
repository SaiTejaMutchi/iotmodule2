## Azure IoT
- Azure IoT is a collection of managed and platform services across edge and cloud that connect, monitor and control billions of IoT assets.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/AzureIoT.png)
### How does it work?
- Azure IoT Hub is Microsoft's Internet of Things connector to the cloud. It's a fully managed cloud service that enables reliable and secure bi-directional communications between millions of IoT devices and a solution back end. Device-to-cloud telemetry data tells you about the state of your devices and assets.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/azure-iot-remotemonitoring-100644066-orig.png)
IoT Central supports the following IoT Edge device patterns:
- edge as leaf device
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/edgeasleafdevice.png) 
- edge with downstream device identity
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/edgewithdownstreamdeviceidentity.png)
- edge withoutdownstream device identity
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/edgewithoutdownstreamdeviceidentity.png)