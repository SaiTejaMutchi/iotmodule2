## InfluxDB
- InfluxDB is an open-source time series database (TSDB) developed by InfluxData.
- nfluxDB is a time series database built specifically for storing time series data, and Grafana is a visualization tool for time series data.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/122.jpg)
### Working
- Telegraf : Data collector written in Go for collecting, processing, and aggregating and writting metrics. Its a plugin driven tool, we will use a few plugins while implementing our use case.
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/working.png)