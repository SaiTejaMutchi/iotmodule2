## AWS IoT
- AWS IoT Core is a managed cloud service that lets connected devices easily and securely interact with cloud applications and other devices. AWS IoT Core can support billions of devices and trillions of messages, and can process and route those messages to AWS endpoints and to other devices reliably and securely.  
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/download.png) 
- MQTT topics identify AWS IoT messages. AWS IoT clients identify the messages they publish by giving the messages topic names. ... AWS IoT uses topics to identify messages received from publishing clients and select messages to send to subscribing clients, as described in the following sections.
### How it works
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/AWS_IoT_Core_-_Connect_and_Manage.edb43e92d542f4053727eaeda267e3776382fd06.png)
- AWS IoT Architecture : 
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/aws-iot-architecture.png)
![](https://gitlab.com/SaiTejaMutchi/iotmodule2/-/raw/master/assignments/extras/111.jpg)
- It is by amazon.